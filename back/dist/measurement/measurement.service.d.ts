import { Model } from 'mongoose';
import { Measurement } from './interfaces/measurement.interface';
export declare class MeasurementService {
    private measurementModel;
    constructor(measurementModel: Model<Measurement>);
    getMeasurementByTag(tagId: any, fini: any, ffin: any): Promise<Measurement[]>;
    getLastReg(tagId: any): Promise<Measurement>;
}
