import { MeasurementService } from './measurement.service';
import { UserService } from '../user/user.service';
export declare class MeasurementController {
    private measurementService;
    private userService;
    constructor(measurementService: MeasurementService, userService: UserService);
    getMeasurement(res: any, req: any, id: string, from: Date, to: Date): Promise<any>;
    getLastReg(res: any, req: any, id: any): Promise<any>;
}
