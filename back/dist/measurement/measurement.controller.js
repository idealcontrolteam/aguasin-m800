"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const measurement_service_1 = require("./measurement.service");
const user_service_1 = require("../user/user.service");
const moment = require("moment");
let MeasurementController = class MeasurementController {
    constructor(measurementService, userService) {
        this.measurementService = measurementService;
        this.userService = userService;
    }
    getMeasurement(res, req, id, from, to) {
        return __awaiter(this, void 0, void 0, function* () {
            let user = req.body;
            if (yield this.userService.validate(user)) {
                if (!id.match(/^[0-9a-fA-F]{24}$/)) {
                    throw new common_1.BadRequestException('Tag id is not a valid ObjectId');
                }
                if (moment(to).diff(moment(from), 'days') > 31) {
                    throw new common_1.BadRequestException('Time range exeeds max allowed, please try less than 31');
                }
                const measurements = yield this.measurementService.getMeasurementByTag(id, from, to);
                let msg = measurements.length == 0
                    ? 'Measurements not found'
                    : 'Measurements fetched';
                return res.status(common_1.HttpStatus.OK).json({
                    statusCode: common_1.HttpStatus.OK,
                    message: msg,
                    data: measurements,
                    count: measurements.length,
                });
            }
            else {
                return res.status(common_1.HttpStatus.UNAUTHORIZED).json({
                    statusCode: common_1.HttpStatus.UNAUTHORIZED,
                    message: 'Not authorized',
                });
            }
        });
    }
    getLastReg(res, req, id) {
        return __awaiter(this, void 0, void 0, function* () {
            let user = req.body;
            if (yield this.userService.validate(user)) {
                if (!id.match(/^[0-9a-fA-F]{24}$/)) {
                    throw new common_1.BadRequestException('Tag id is not a valid ObjectId');
                }
                const measurement = yield this.measurementService.getLastReg(id);
                let msg = !measurement ? 'Measurement not found' : 'Measurement fetched';
                return res.status(common_1.HttpStatus.OK).json({
                    statusCode: common_1.HttpStatus.OK,
                    message: msg,
                    data: measurement,
                });
            }
            else {
                return res.status(common_1.HttpStatus.UNAUTHORIZED).json({
                    statusCode: common_1.HttpStatus.UNAUTHORIZED,
                    message: 'Not authorized',
                });
            }
        });
    }
};
__decorate([
    common_1.Post('/:from/:to/:id'),
    __param(0, common_1.Res()),
    __param(1, common_1.Req()),
    __param(2, common_1.Param('id')),
    __param(3, common_1.Param('from')),
    __param(4, common_1.Param('to')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, String, Date,
        Date]),
    __metadata("design:returntype", Promise)
], MeasurementController.prototype, "getMeasurement", null);
__decorate([
    common_1.Post('/lastReg/:id'),
    __param(0, common_1.Res()), __param(1, common_1.Req()), __param(2, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementController.prototype, "getLastReg", null);
MeasurementController = __decorate([
    common_1.Controller('/tag/measurement'),
    __metadata("design:paramtypes", [measurement_service_1.MeasurementService,
        user_service_1.UserService])
], MeasurementController);
exports.MeasurementController = MeasurementController;
//# sourceMappingURL=measurement.controller.js.map