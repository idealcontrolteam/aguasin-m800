"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.UserSchema = new mongoose_1.Schema({
    name: String,
    email: String,
    username: String,
    password: String,
    active: Boolean,
    lastConnection: {
        type: Date,
        default: null,
    },
}, { versionKey: false });
//# sourceMappingURL=user.schema.js.map