import { Model } from 'mongoose';
import { User } from './interfaces/user.interface';
export declare class UserService {
    private userModel;
    constructor(userModel: Model<User>);
    login(user: any): Promise<User>;
    validate(user: any): Promise<boolean>;
}
