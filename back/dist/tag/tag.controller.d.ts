import { TagService } from './tag.service';
import { UserService } from '../user/user.service';
export declare class TagController {
    private tagService;
    private userService;
    constructor(tagService: TagService, userService: UserService);
    getTags(res: any, req: any): Promise<any>;
    getTag(res: any, req: any, id: any): Promise<any>;
}
