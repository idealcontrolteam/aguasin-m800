import { Model } from 'mongoose';
import { Tag } from './interfaces/tag.interface';
export declare class TagService {
    private tagModel;
    constructor(tagModel: Model<Tag>);
    getTags(): Promise<Tag[]>;
    getTag(id: string): Promise<Tag>;
}
