"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const tag_service_1 = require("./tag.service");
const user_service_1 = require("../user/user.service");
let TagController = class TagController {
    constructor(tagService, userService) {
        this.tagService = tagService;
        this.userService = userService;
    }
    getTags(res, req) {
        return __awaiter(this, void 0, void 0, function* () {
            let user = req.body;
            if (yield this.userService.validate(user)) {
                const tags = yield this.tagService.getTags();
                let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';
                return res.status(common_1.HttpStatus.OK).json({
                    statusCode: common_1.HttpStatus.OK,
                    message: msg,
                    data: tags,
                    count: tags.length,
                });
            }
            else {
                return res.status(common_1.HttpStatus.UNAUTHORIZED).json({
                    statusCode: common_1.HttpStatus.UNAUTHORIZED,
                    message: 'Not authorized',
                });
            }
        });
    }
    getTag(res, req, id) {
        return __awaiter(this, void 0, void 0, function* () {
            let user = req.body;
            if (yield this.userService.validate(user)) {
                if (!id.match(/^[0-9a-fA-F]{24}$/)) {
                    throw new common_1.BadRequestException('Tag id is not a valid ObjectId');
                }
                const tag = yield this.tagService.getTag(id);
                if (!tag) {
                    throw new common_1.NotFoundException('Tag not found');
                }
                return res.status(common_1.HttpStatus.OK).json({
                    statusCode: common_1.HttpStatus.OK,
                    message: 'Tag found',
                    data: tag,
                });
            }
            else {
                return res.status(common_1.HttpStatus.UNAUTHORIZED).json({
                    statusCode: common_1.HttpStatus.UNAUTHORIZED,
                    message: 'Not authorized',
                });
            }
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TagController.prototype, "getTags", null);
__decorate([
    common_1.Post('/:id'),
    __param(0, common_1.Res()), __param(1, common_1.Req()), __param(2, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], TagController.prototype, "getTag", null);
TagController = __decorate([
    common_1.Controller('tag'),
    __metadata("design:paramtypes", [tag_service_1.TagService, user_service_1.UserService])
], TagController);
exports.TagController = TagController;
//# sourceMappingURL=tag.controller.js.map