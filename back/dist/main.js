"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const common_1 = require("@nestjs/common");
const rateLimit = require("express-rate-limit");
function bootstrap() {
    return __awaiter(this, void 0, void 0, function* () {
        const app = yield core_1.NestFactory.create(app_module_1.AppModule);
        const rate = 1;
        const points = 5;
        app.enableCors();
        app.useGlobalPipes(new common_1.ValidationPipe());
        app.use(rateLimit({
            windowMs: rate * 60 * 1000,
            max: points,
            message: {
                statusCode: 429,
                message: "Too many accounts created from this IP, please try again after an minute"
            }
        }));
        yield app.listen(3008);
    });
}
bootstrap();
//# sourceMappingURL=main.js.map