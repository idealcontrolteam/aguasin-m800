import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Tag } from './interfaces/tag.interface';
import {_} from 'lodash';


@Injectable()
export class TagService {
  constructor(
    @InjectModel('Tag') private tagModel: Model<Tag>
  ) {}
  async getTags(): Promise<Tag[]> {
    const tags = await this.tagModel.find({address: { $gte: 301, $lte: 304 }},
      ['_id','alertMax','alertMin','dateTimeLastValue','lastValue','name','nameAddress','shortName','unity']);
    return tags;
  }
  async getTag(id:string): Promise<Tag> {
    const tag = await this.tagModel.findById(id,
      ['_id','alertMax','alertMin','dateTimeLastValue','lastValue','name','nameAddress','shortName','unity']);
    return tag;
  }
}
