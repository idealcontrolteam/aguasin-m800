import { Controller, Get, Res, HttpStatus, Param, NotFoundException, BadRequestException, Req, Post } from '@nestjs/common';
import { TagService } from './tag.service';
import { UserService } from '../user/user.service';

@Controller('tag')
export class TagController {
  constructor(private tagService: TagService, private userService:UserService) {}
  @Post()
  async getTags(@Res() res, @Req() req) {
    let user=req.body;
    if(await this.userService.validate(user)){
      const tags = await this.tagService.getTags();
      let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: msg,
        data: tags,
        count: tags.length,
      });
    }else{
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    }
  }
  @Post('/:id')
  async getTag(@Res() res, @Req() req, @Param('id') id) {
    let user=req.body;
    if(await this.userService.validate(user)){
      if (!id.match(/^[0-9a-fA-F]{24}$/)) {
        throw new BadRequestException('Tag id is not a valid ObjectId');
      }
      const tag = await this.tagService.getTag(id);
      if (!tag) {
        throw new NotFoundException('Tag not found');
      }
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'Tag found',
        data: tag,
      });
    }else{
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    }
  }
}