import { Module } from '@nestjs/common';
import { TagController } from './tag.controller';
import { TagService } from './tag.service';
import { MongooseModule } from '@nestjs/mongoose';
import { TagSchema } from './schemas/tag.schema';
import { UserService } from '../user/user.service';
import { UserModule } from '../user/user.module';

@Module({
  imports: [
    UserModule,
    MongooseModule.forFeature([
      { name: 'Tag', schema: TagSchema, collection: 'tag' },
    ]),
  ],
  controllers: [TagController],
  providers: [
    TagService,
    UserService,
  ],
  exports: [TagService],
})
export class TagModule {}
