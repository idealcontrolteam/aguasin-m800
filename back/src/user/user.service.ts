import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './interfaces/user.interface';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class UserService {
  constructor(@InjectModel('User') private userModel: Model<User>) {}

  async login(user): Promise<User> {
    const { email } = user;
    const userDB = await this.userModel.findOne({ email });
    return userDB;
  }
  async validate(user) {
    const { email } = user;
    const userDB = await this.userModel.findOne({ email });
    const compare = userDB ? await bcrypt.compare(user.password, userDB.password) : null;
    return compare;
  }
}
