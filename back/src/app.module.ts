import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './user/user.module';
import { MeasurementModule } from './measurement/measurement.module';
import { TagModule } from './tag/tag.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://idealcontrol:Ideal_cloud281@169.53.138.44:27017/bdaguasin?authSource=admin', {
    // MongooseModule.forRoot('mongodb://127.0.0.1:27017/bdaguasin', {
      useNewUrlParser: true,
      useFindAndModify: false,
    }),
    UserModule,
    MeasurementModule,
    TagModule,
  ],
})
export class AppModule {}