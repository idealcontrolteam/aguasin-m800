import { Module } from '@nestjs/common';
import { MeasurementController } from './measurement.controller';
import { MeasurementService } from './measurement.service';
import { UserModule } from '../user/user.module';
import { UserService } from '../user/user.service';
import { MongooseModule } from '@nestjs/mongoose';
import { MeasurementSchema } from './schemas/measurement.schema';


@Module({
  imports: [
    UserModule,
    MongooseModule.forFeature([
      {
        name: 'Measurement',
        schema: MeasurementSchema,
        collection: 'measurement',
      },
    ]),
  ],
  controllers: [MeasurementController],
  providers: [MeasurementService, UserService],
  exports: [MeasurementService],
})
export class MeasurementModule {}
