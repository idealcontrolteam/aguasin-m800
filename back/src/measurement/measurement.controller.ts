import {
  Controller,
  Res,
  HttpStatus,
  Param,
  BadRequestException,
  Post,
  Req,
} from '@nestjs/common';
import { MeasurementService } from './measurement.service';
import { UserService } from '../user/user.service';
import * as moment from 'moment'

@Controller('/tag/measurement')
export class MeasurementController {
  constructor(
    private measurementService: MeasurementService,
    private userService: UserService,
  ) {}

  @Post('/:from/:to/:id')
  async getMeasurement(
    @Res() res,
    @Req() req,
    @Param('id') id: string,
    @Param('from') from: Date,
    @Param('to') to: Date,
  ) {
    let user = req.body;
    if (await this.userService.validate(user)) {
      if (!id.match(/^[0-9a-fA-F]{24}$/)) {
        throw new BadRequestException('Tag id is not a valid ObjectId');
      }
      if(moment(to).diff(moment(from), 'days')>31){
        throw new BadRequestException('Time range exeeds max allowed, please try less than 31');
      }

      const measurements = await this.measurementService.getMeasurementByTag(
        id,
        from,
        to,
      );
      let msg =
        measurements.length == 0
          ? 'Measurements not found'
          : 'Measurements fetched';
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: msg,
        data: measurements,
        count: measurements.length,
      });
    } else {
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    }
  }
  @Post('/lastReg/:id')
  async getLastReg(@Res() res, @Req() req, @Param('id') id) {
    let user = req.body;
    if (await this.userService.validate(user)) {
      if (!id.match(/^[0-9a-fA-F]{24}$/)) {
        throw new BadRequestException('Tag id is not a valid ObjectId');
      }
      const measurement = await this.measurementService.getLastReg(id);
      let msg = !measurement ? 'Measurement not found' : 'Measurement fetched';
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: msg,
        data: measurement,
      });
    } else {
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    }
  }
}
