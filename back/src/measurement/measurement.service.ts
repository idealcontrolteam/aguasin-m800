import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Measurement } from './interfaces/measurement.interface';


@Injectable()
export class MeasurementService {
  constructor(
    @InjectModel('Measurement') private measurementModel: Model<Measurement>,
  ) {}

  async getMeasurementByTag(tagId, fini, ffin): Promise<Measurement[]> {
    const measurements = await this.measurementModel.find({
      tagId: tagId,
      dateTime: { $gte: fini, $lte: ffin },
    }).sort({dateTime: 1});
    return measurements;
  }
  async getLastReg(tagId): Promise<Measurement> {
    const measurements = await this.measurementModel.findOne({
      tagId: tagId
    },['tagId','value','dateTime'])
    .sort({dateTime: -1});
    return measurements;
  }
}
