[![Idealcontrol](http://idealcontrol.cl/webwp/wp-content/uploads/2018/04/logo-solo-180largo.png)](http://idealcontrol.cl/webwp/wp-content/uploads/2018/04/logo-solo-180largo.png)

# API M800 v2

## Objetivo del documento
Informar al cliente sobre las URL que dispondrá en la presente API, adjuntando además los parámetros y resultados de cada una de ellas

## Endpoints y Validación

**Todos los endpoints requieren enviar por POST un body en formato JSON para validar el usuario**
```
{
    "email":string,
    "password":string
}
```
* _email_ : dirección asociada al ingreso del sistema
* _password_ : contraseña asociada a la cuenta de ingreso

**De lo contrario las solicitudes serán rechazadas**
```
{
    "statusCode": 401,
    "message": "Not authorized"
}
```

**Se acepta un máximo de 5 consultas por minuto. Posterior a esto, serán rechazadas**
```
{
    "statusCode": 429,
    "message": "Too many accounts created from this IP, please try again after an minute"
}
```

### Obtener listado de tags
```
POST - https://ihc.idealcontrol.cl/m800/apiv2/tag
```

#### Ejemplo
```
https://ihc.idealcontrol.cl/m800/apiv2/tag
```

#### Body
```
{
	"email":"userm800@idealcontrol.cl", 
	"password":"userm800"
}
```

#### Resultado
```
{
    "statusCode": 200,
    "message": "Tags fetched",
    "data": [
        {
            "_id": "5dfa743969e895a6c90005a5",
            "alertMax": 25,
            "alertMin": 0,
            "dateTimeLastValue": "2020-03-20T18:43:00.000Z",
            "lastValue": 20.3,
            "name": "EFL SS.",
            "nameAddress": "SolidSusp1S1",
            "shortName": "EFL SS.",
            "unity": "ppm"
        },
        {
            "_id": "5dfa743969e895a6c90005a6",
            "alertMax": 25,
            "alertMin": 0,
            "dateTimeLastValue": "2020-03-20T18:43:00.000Z",
            "lastValue": 10.7,
            "name": "EFL TURB.",
            "nameAddress": "Turb.1S2",
            "shortName": "EFL TURB.",
            "unity": "ntu"
        },
        {...}
    ],
    "count": 24
}
```
* _satusCode_ : estado de la consulta
* _message_ : mensaje descriptor de la consulta realizada
* _data_ : conjunto de tags con todos sus registros
* _count_ : cantidad de tags encontrados

### Obtener información de un tag específico
```
POST - https://ihc.idealcontrol.cl/m800/apiv2/tag/:id
```
* _id_ : id de registro del tag

#### Ejemplo
```
https://ihc.idealcontrol.cl/m800/apiv2/tag/5dfa743969e895a6c90005a5
```

#### Body
```
{
	"email":"userm800@idealcontrol.cl", 
	"password":"userm800"
}
```

#### Resultado
```
{
    "statusCode": 200,
    "message": "Tag found",
    "data": {
        "_id": "5dfa743969e895a6c90005a5",
        "alertMax": 25,
        "alertMin": 0,
        "dateTimeLastValue": "2020-03-20T18:43:00.000Z",
        "lastValue": 20.3,
        "name": "EFL SS.",
        "nameAddress": "SolidSusp1S1",
        "shortName": "EFL SS.",
        "unity": "ppm"
    }
}
```
* _satusCode_ : estado de la consulta
* _message_ : mensaje descriptor de la consulta realizada
* _data_ : datos actuales del tag consultado

### Obtener registros de un tag específico en un rango de fechas
```
POST - https://ihc.idealcontrol.cl/m800/apiv2/tag/measurement/:from/:to/:id
```
* _from_ : fecha inicial en formato YYYY-MM-DD
* _to_ : fecha final en formato YYYY-MM-DD _(máximo 31 días a partir de la fecha de inicio)_
* _id_ : id de registro del tag

#### Ejemplo
```
https://ihc.idealcontrol.cl/m800/apiv2/tag/measurement/2020-03-01/2020-03-02/5dfa743969e895a6c90005a5
```

#### Body
```
{
	"email":"userm800@idealcontrol.cl", 
	"password":"userm800"
}
```

#### Resultado
```
{
    "statusCode": 200,
    "message": "Measurements fetched",
    "data": [
        
        {
            "_id": "5e5b2507d7320d77e0ad0adc",
            "tagId": "5dfa743969e895a6c90005a5",
            "dateTime": "2020-03-01T00:00:00.000Z",
            "locationId": "5dfa83a469e895a6c90005b9",
            "value": 20.6,
            "active": true
        },
        {
            "_id": "5e5b266c86c96c5aa52c659f",
            "tagId": "5dfa743969e895a6c90005a5",
            "dateTime": "2020-03-01T00:05:00.000Z",
            "locationId": "5dfa83a469e895a6c90005b9",
            "value": 20.6,
            "active": true
        },
        {...}
    ],
    "count": 290
}
```
* _satusCode_ : estado de la consulta
* _message_ : mensaje descriptor de la consulta realizada
* _data_ : conjunto de registros generados por un tag en el rango de tiempo consultado
* _count_ : cantidad de registros encontrados

### Obtener el último registro de un tag específico
```
POST - https://ihc.idealcontrol.cl/m800/apiv2/tag/measurement/lastReg/:id
```
* _id_ : id de registro del tag

#### Ejemplo
```
https://ihc.idealcontrol.cl/m800/apiv2/tag/measurement/lastReg/5dfa743969e895a6c90005a5
```

#### Body
```
{
	"email":"userm800@idealcontrol.cl", 
	"password":"userm800"
}
```

#### Resultado
```
{
    "statusCode": 200,
    "message": "Measurement fetched",
    "data": {
        "_id": "5e73cf9152f9bc74cc9ad4e6",
        "dateTime": "2020-03-19T17:00:00.000Z",
        "value": 36.2
    }
}
```
* _satusCode_ : estado de la consulta
* _message_ : mensaje descriptor de la consulta realizada
* _data_ : id, valor y fecha del último registro generado por un tag